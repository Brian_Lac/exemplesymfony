<?php

namespace App\Controller;

use App\Entity\Technique;
use App\Form\TechniqueType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/techniques", name="techniques_")
 */
class TechniqueController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(): Response
    {
        $technique = $this->getDoctrine()->getRepository(Technique::class)->findAll();       

        return $this->render('technique/index.html.twig', [
            'techniques' => $technique
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $technique = new Technique();
        $form = $this->createForm(TechniqueType::class, $technique);
        $form->handleRequest($request);

        // Validation formulaire
        if($form->isSubmitted() && $form->isValid()){

            $doctrine = $this->getDoctrine()->getManager();

            // ajouts de la photo
            if($technique->getImage()){
                $photo = $form->get('image')->getData();

                $nouveau_nom = md5(uniqid()).'.'.$photo->guessExtension();

                $photo->move(
                    $this->getParameter('fiches'),
                    $nouveau_nom
                );
                $technique->setImage($nouveau_nom);
            }
            $technique->setDate(new \DateTimeImmutable());

            // ajouts des des allergenes pour chaques ingrédient
            foreach($form->get('ingredients')->getData() as $ingredient) {
                $technique->addIngredient($ingredient);
                $ingredient->setRecette($technique);
                $produit=$ingredient->getProduits();
                foreach ($produit->getAllergenes() as $prodallergenes) {
                    $technique->addAllergene($prodallergenes);  
                }
            }
            // calcul du total
            $total=0;
            $ingredients=$technique->getIngredients();
            foreach ($ingredients as $ingredient ) {
                $total+= $ingredient->getProduits()->getPrix() * $ingredient->getQuantity();
            }
            
            $technique->setPrix($total);
            $doctrine->persist($technique);
            $doctrine->flush();

            $this->addFlash('success', 'Votre fiche technique à bien été crée !');

        return $this->redirectToRoute('techniques_index');
        }

        return $this->render('technique/new.html.twig', [
            'technique' => $technique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(Technique $technique): Response
    {
        return $this->render('technique/show.html.twig', [
            'technique' => $technique,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Technique $technique): Response
    {   
        //Sauvegarde ancienne photo
        $ancienne_photo=$technique->getImage();
        
        $form = $this->createForm(TechniqueType::class, $technique);
        $form->handleRequest($request);
       
        // Validation formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            // edit de la photo
            if($technique->getImage()){
                if($ancienne_photo != null){
                    $filesystem = new Filesystem();
                    $filesystem->remove('upload/images/fiches/'.$ancienne_photo);
                }
                $photo = $form->get('image')->getData();

                $nouveau_nom = md5(uniqid()).'.'.$photo->guessExtension();

                $photo->move(
                    $this->getParameter('fiches'),
                    $nouveau_nom
                );
                $technique->setImage($nouveau_nom);
            }else{
                $technique->setImage($ancienne_photo);
            }
            $technique->setDate(new \DateTimeImmutable());

            // ajouts  des allergenes pour chaques ingrédient
            foreach($form->get('ingredients')->getData() as $ingredient) {
                $technique->addIngredient($ingredient);
                $ingredient->setRecette($technique);
                $produit=$ingredient->getProduits();
                foreach ($produit->getAllergenes() as $prodallergenes) {
                    $technique->addAllergene($prodallergenes);  
                }
            }

            // Calcul du prix total
            $total=0;
            $ingredients=$technique->getIngredients();
            foreach ($ingredients as $ingredient ) {
                $total+= $ingredient->getProduits()->getPrix() * $ingredient->getQuantity();
            }
            $technique->setPrix($total);

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Votre fiche technique à bien été modifiée !');

            return $this->redirectToRoute('techniques_index');
        }

        return $this->render('technique/edit.html.twig', [
            'technique' => $technique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/del/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, Technique $technique): Response
    {
        if ($this->isCsrfTokenValid('delete'.$technique->getId(), $request->request->get('_token'))) {
            // Suppr Image si il y a
            if($technique->getImage()){
            
                $filesystem = new Filesystem();
                $filesystem->remove('upload/images/fiches/'.$technique->getImage());
    
            }

            $entityManager = $this->getDoctrine()->getManager();

            // Suppr tout les ingredients de la technique
            foreach ($technique->getIngredients() as $ingredient) {
                $entityManager->remove($ingredient);
            }
            
            $entityManager->remove($technique);
            $entityManager->flush();

            $this->addFlash('success', 'Votre fiche technique à bien été supprimé !');
        }

        return $this->redirectToRoute('techniques_index');

    } 
}
