<?php

namespace App\Controller;

use App\Entity\Produits;
use App\Form\ProduitsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/produits", name="produits_")
 */
class ProduitsController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {   
        $produits = $this->getDoctrine()->getRepository(Produits::class)->findAll();       
 
        return $this->render('produits/index.html.twig', [
            'produits' => $produits,
        ]);
    }

    /**
     * @Route("/new", name="new")
     */
    public function new(Request $request): Response
    {
        $produit = new Produits();
        $form = $this->createForm(ProduitsType::class, $produit);
        $form->handleRequest($request);

        // Validation formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            $date=new \DateTime();
            $produit->setDate($date);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($produit);
            $entityManager->flush();
            $this->addFlash('success', 'Le produit à bien été crée !');
            return $this->redirectToRoute('produits_index');
        }

        return $this->render('produits/new.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show")
     */
    public function show(Produits $produit): Response
    {
        return $this->render('produits/show.html.twig', [
            'produit' => $produit,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit")
     */
    public function edit(Request $request, Produits $produit): Response
    {
        $form = $this->createForm(ProduitsType::class, $produit);
        $form->handleRequest($request);

        // Validation formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            $date=new \DateTime();
            $produit->setDate($date);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Le produit à bien été modifié !');

            return $this->redirectToRoute('produits_index');
        }

        return $this->render('produits/edit.html.twig', [
            'produit' => $produit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/del", name="delete")
     */
    public function delete(Request $request, Produits $produit): Response
    {
        if ($this->isCsrfTokenValid('delete'.$produit->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();

            // check si il ya des techniques avec ce produits 

            foreach ($produit->getIngredients() as $ingredient) {
                if($ingredient){
                    $this->addFlash('warning', 'Le produit ne peut pas etre supprimer ! il faut supprimer les recettes avec ce produit avant');
                    return $this->redirectToRoute('produits_index');
                }
            }
            $entityManager->remove($produit);
            $entityManager->flush();
            $this->addFlash('success', 'Le produit à bien été supprimé !');
            return $this->redirectToRoute('produits_index');
        
            
        }
        
        return $this->redirectToRoute('produits_index');
    }
}
