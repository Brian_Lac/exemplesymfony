<?php

namespace App\Form;

use App\Entity\Produits;
use App\Entity\Unite;
use App\Entity\Stockages;
use App\Entity\Origines;
use App\Entity\Fournisseurs;
use App\Entity\Allergenes;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProduitsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class, array(
                'label' => 'Ecrivez votre Nom de produit',
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Votre Nom de produit ne peut pas etre vide !'
                    ))
                )))
            ->add('prix',NumberType::Class, array(
                'label' => 'Choississez votre Prix',
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Votre prix ne peut pas etre vide !'
                    ))
                )))
            ->add('info',TextareaType::class, array(
                'required'=> false,
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'label' => 'Ecrivez vos infos complémentaire',
                ))
            ->add('unite',EntityType::Class, array(
                'class' => Unite::class,
                'attr'=> array(
                    'class'=>'form-control selected2'
                ),
                'choice_label' => 'nom',
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Selectionner  une categorie'
                    ])
                ]
                ))
            ->add('stockage',EntityType::Class, array(
                'class' => Stockages::class,
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'choice_label' => 'nom',
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Selectionner  un stockage'
                    ])
                ]
                ))
            ->add('famille',EntityType::Class, array(
                'required' => false,
                'class' => Origines::class,
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'choice_label' => 'nom',
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Selectionner une famille'
                    ])
                ]
                ))
            ->add('fournisseur',EntityType::Class, array(
                'class' => Fournisseurs::class,
                'attr'=> array(
                    'class'=>'form-control selected2'
                ),
                'choice_label' => 'nom',
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Selectionner un fournisseur'
                    ])
                ]
                ))
            ->add('allergenes',EntityType::Class, array(
                'required' => false,
                'class' => Allergenes::class,
                'attr'=> array(
                    'class'=>'form-control selected3'
                ),
                'choice_label' => 'nom',
                'multiple' => true,
                ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Produits::class,
        ]);
    }
}
