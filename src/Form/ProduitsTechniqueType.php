<?php

namespace App\Form;

use App\Entity\Produits;
use App\Entity\Ingredient;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class ProduitsTechniqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('produits',EntityType::class, array(
                'class' => Produits::class,
                'choice_label' => 'nom',
                'attr'=> array(
                    'class'=>'form-control selected2'
                ),
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Selectionner une categorie'
                    ])
                ]
                ))
            ->add('quantity', NumberType::class, array(
                'label' => 'Quantité',
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Selectionner une quantité'
                    ])
                ]
                ))
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ingredient::class,
        ]);
    }
}
