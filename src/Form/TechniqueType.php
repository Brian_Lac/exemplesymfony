<?php

namespace App\Form;

use App\Entity\Classement;
use App\Entity\Allergenes;
use App\Entity\Technique;
use Doctrine\DBAL\Types\FloatType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class TechniqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class, array(
                'label' => 'Entrez un nom',
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Le Nom ne peut pas etre vide !'
                    ))
                )))
            ->add('portions',TextType::class, array(
                'label' => 'Nombre de portions',
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Le nombre de portions est obligatoire !'
                    ))
                )))
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => 'Description',
                'attr'=> array(
                    'class'=>'form-control'
                )
            ])
            ->add('image', FileType::class, array(
                'required' => false,
                'label' => 'Choisir une photo',
                'data_class' => null,
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'constraints' => [
                    new image ([
                        'mimeTypes' => [
                            'image/png', 'image/jpeg', 'image/gif'
                        ]
                    ])
                ]
            ))
            ->add('progression', TextareaType::class, [
                'required' => true,
                'label' => 'Progression',
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'constraints' => [
                    new NotBlank(array(
                        'message' => 'La progression est obligatoire !'
                    ))
                ]
            ])
            ->add('point_crit', TextareaType::class, [
                'required' => false,
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'label' => 'Point critique',
            ])
            ->add('allergenes', EntityType::class, array(
                'required' => false,
                'attr'=> array(
                    'class'=>'form-control selected3'
                ),
                'class' => Allergenes::class,
                'choice_label' => 'nom',
                'multiple' => true
                ))
            ->add('classement', EntityType::class, array(
                'class' => Classement::class,
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'choice_label' => 'nom'
                ))
            ->add('ingredients', CollectionType::class, array(
                'entry_type' => ProduitsTechniqueType::class,
                'attr'=> array(
                    'class'=>'col-12'
                ),
                'allow_add' => true,
                'allow_delete' => true
                ))
            ->add('froid', CheckboxType::class,array(
                'attr'=> array(
                    'class'=>'form-control'
                ),
                'label'=> 'Garder au froid?',
                'required' => false
                ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Technique::class,
        ]);
    }
}
