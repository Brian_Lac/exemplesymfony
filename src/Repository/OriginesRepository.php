<?php

namespace App\Repository;

use App\Entity\Origines;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Origines|null find($id, $lockMode = null, $lockVersion = null)
 * @method Origines|null findOneBy(array $criteria, array $orderBy = null)
 * @method Origines[]    findAll()
 * @method Origines[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OriginesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Origines::class);
    }

    // /**
    //  * @return Origines[] Returns an array of Origines objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Origines
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
