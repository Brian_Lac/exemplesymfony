<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class Origines extends Fixture
{   
    
    public function load(ObjectManager $manager)
    {
        $origines=array('Agneau','Boeuf','Charcuterie','Poisson','Fruits de mer','Porc','Veau','Volaille');
        $i=1;
        // Ajout de plusieurs éléments en BDD
        foreach ($origines as $origine) {
            $neworigine = new \App\Entity\Origines();
            $neworigine->setNom($origine);
            $manager->persist($neworigine);
            // Enregistrement de l'article en référence
            $this->addReference('origine_'. $i, $neworigine);
            $i++;
        } 
        // Insertion en BDD
        $manager->flush();
    }
    public function getOrder()
    {
        return 1;
    }
}
