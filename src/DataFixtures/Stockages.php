<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class Stockages extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $stockages=array('BOF','Boulangerie','Cave','Epicerie','F&L Frais','Produits frais','Surgelés','Produits entretient');
        $i=1;
        // Ajout de plusieurs éléments en BDD
        foreach ($stockages as $stockage) {
            $newstockage = new \App\Entity\Stockages();
            $newstockage->setNom($stockage);
            $manager->persist($newstockage);
            // Enregistrement de l'article en référence
            $this->addReference('stockage_'. $i, $newstockage);
            $i++;
        } 
        // Insertion en BDD
        $manager->flush();
    }
    public function getOrder()
    {
        return 1;
    }
}
