<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class Allergenes extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $allergenes=array('Gluten','Crustacés','Oeufs','Poissons','Arachides','Soja','Lait','Fruits à coques','Céleri','Moutarde','Graines de sésames','Anhydride sulfureux et sulfites','Lupin','Mollusques');
        $i=1;
        // Ajout de plusieurs éléments en BDD
        foreach ($allergenes as $allergene) {
            $newallergene = new \App\Entity\Allergenes();
            $newallergene->setNom($allergene);
            $manager->persist($newallergene);
            // Enregistrement de l'article en référence
            $this->addReference('allergene_'. $i, $newallergene);
            $i++;
        } 
        // Insertion en BDD
        $manager->flush();
    }
    public function getOrder()
    {
        return 1;
    }
}
