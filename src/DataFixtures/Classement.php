<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class Classement extends Fixture
{
    public function load(ObjectManager $manager)
    {   
        $classements=array('Amuse Bouche','Entrée froide','Entrée chaude','Viande','Volaille','Plats Complet','Dessert');
        $i=1;
        // Ajout de plusieurs éléments en BDD
        foreach ($classements as $classement) {
            $newclassement = new \App\Entity\Classement();
            $newclassement->setNom($classement);
            $manager->persist($newclassement);
            // Enregistrement de l'article en référence
            $this->addReference('classement_'. $i, $newclassement);
            $i++;
        } 
        // Insertion en BDD
        $manager->flush();
    }
    public function getOrder()
    {
        return 1;
    }
}
