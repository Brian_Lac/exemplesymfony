<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class Fournisseurs extends Fixture
{
    public function load(ObjectManager $manager)
    {

        // Initialisation du bundle Faker
        $faker = Faker\Factory::create('fr_FR');

        // Ajout de plusieurs éléments en BDD
        for($i = 1; $i <= 10; $i++) {
            $fournisseur = new \App\Entity\Fournisseurs();
            $fournisseur->setNom($faker->company);
            
            $manager->persist($fournisseur);

            // Enregistrement de l'fournisseur en référence
            $this->addReference('fournisseur_'. $i, $fournisseur);
    
        }

        // Insertion en BDD
        $manager->flush();

    }
    public function getOrder()
    {
        return 1;
    }
}
