<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class Unite extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $unites=array('Boite','Botte','Carton','Un','Litre','Kg','Lot','Paquet','Pièce','Pot','Rouleau','Sac');
        $i=1;
        // Ajout de plusieurs éléments en BDD
        foreach ($unites as $unite) {
            $newunite = new \App\Entity\Unite();
            $newunite->setNom($unite);
            $manager->persist($newunite);
            // Enregistrement de l'article en référence
            $this->addReference('unite_'. $i, $newunite);
            $i++;
        } 
        // Insertion en BDD
        $manager->flush();
    }
    public function getOrder()
    {
        return 1;
    }
}
