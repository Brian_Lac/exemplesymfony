<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

 class xProduits extends Fixture
{   
    public function load(ObjectManager $manager)
    {
    // Initialisation du bundle Faker
    $faker = Faker\Factory::create('fr_FR');

    // Ajout de plusieurs éléments en BDD
    for($i = 1; $i <= 50; $i++) {

        
        $produit = new \App\Entity\Produits();
        $produit->setNom($faker->jobTitle);
        $produit->setDate($faker->dateTime);
        $produit->setPrix($faker->numberBetween(1, 100));
        $produit->setInfo($faker->realText(50));
        $produit->setUnite($this->getReference('unite_'. $faker->numberBetween(1, 12)));
        $produit->setStockage($this->getReference('stockage_'. $faker->numberBetween(1, 8)));
        $produit->setFamille($this->getReference('origine_'. $faker->numberBetween(1, 8)));
        $produit->setFournisseur($this->getReference('fournisseur_'. $faker->numberBetween(1,10 )));

        // Enregistre une ou plusieurs catégories
        for($j = 0; $j <= $faker->numberBetween(1, 3); $j++) {
            $produit->addAllergene($this->getReference('allergene_'. $faker->numberBetween(1, 14)));
        }

        $manager->persist($produit);

        // Enregistrement de l'produit en référence
        $this->addReference('produit_'. $i, $produit);
    }

    // Insertion en BDD
    $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}


